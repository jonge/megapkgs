let
  sources = import ./nix/sources.nix {};

  megaOverlays =
    let
      projects = import ./projects.nix sources;
    in
      map
        (x: import "${x}/overlay.nix")
        (builtins.attrValues projects)
      ++ [ (import ./overlay.nix) ];
in

args: import sources.nixpkgs (args or {} // {
  overlays = args.overlays or [] ++ megaOverlays;
}
