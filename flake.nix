{
  description = "Description for the project";

  inputs = {
    flake-parts.url = "github:hercules-ci/flake-parts";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    project_a.url = "git+https://gitlab.com/jonge/megacorp-a";
    project_a.inputs.nixpkgs.follows = "nixpkgs";
    project_b.url = "git+https://gitlab.com/jonge/megacorp-b";
    project_b.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      systems = [ "x86_64-linux" "aarch64-linux" "aarch64-darwin" "x86_64-darwin" ];
      perSystem = { config, self', inputs', pkgs, system, ... }: {
        _module.args.pkgs = import inputs.nixpkgs {
          inherit system;
          overlays = [ inputs.self.overlays.default ];
          config = { };
        };
        packages = {
          inherit (pkgs)
            project_a
            project_b
            ;
        };

        checks = pkgs.megacorp.tests;
      };
      flake = {
        overlays.default = inputs.nixpkgs.lib.composeManyExtensions
          (with inputs.self.overlays; [ self project_a project_b ]);
        overlays.self = import ./overlay.nix;
        overlays.project_a = inputs.project_a.overlays.default;
        overlays.project_b = inputs.project_b.overlays.default;
      };
    };
}
