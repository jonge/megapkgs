let
  sources = import ./nix/sources.nix {};
  projects = import ./projects.nix sources;
in
  map
    (x: "${x}/module.nix")
    (builtins.attrValues projects)
