final: prev:
let
  sources = import ./nix/sources.nix {};

  modules =
    let
      projects = import ./projects.nix sources;
    in
      map
        (x: "${x}/module.nix")
        (builtins.attrValues projects);
in
{
  megacorp = prev.megacorp or {} // {
    inherit modules;
    nixos = ms: final.nixos (modules ++ ms);
  };
}
