let
  sources = import ./nix/sources.nix {};

  megaOverlays =
    let
      projects = import ./projects.nix sources;
    in
      map
        (x: import "${x}/overlay.nix")
        (builtins.attrValues projects)
      ++ [ (import ./overlay.nix) ];

  pkgs = import sources.nixpkgs {
    overlays = megaOverlays;
  };

in

{
  inherit megaOverlays;
  inherit (pkgs) project_a project_b;

  tests = pkgs.megacorp.tests // {
    test-ab = pkgs.testers.runNixOSTest ./test-ab.nix;
  };

  inherit pkgs;

  devShell = pkgs.mkShell {
    nativeBuildInputs = with pkgs; [
      niv
    ];
  };
}
