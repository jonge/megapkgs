{
  name = "Bestest integratoin test of the world";

  extraBaseModules = {
    imports = import ./modules.nix;
  };

  nodes = {
    server = { config, pkgs,  ... }: {
      services.project_b.enable = true;
      services.project_a = {
        enable = true;
        unixSocketPath =
          config.services.project_b.unixSocketPath;
      };
      networking.firewall.enable = false;
    };

    client = {};
  };

  testScript = { nodes, ... }: ''
    start_all()

    PORT = ${builtins.toString nodes.server.services.project_a.port}

    server.wait_for_unit("network-online.target")
    server.wait_for_unit("project_a.service")
    server.wait_for_unit("project_b.service")
    server.wait_for_open_port(PORT)

    client.wait_for_unit("network-online.target")
    input_string = "foobar lol bamboozled lel"
    output = client.succeed(f"echo {input_string} | nc server {PORT}", timeout=3)
    assert input_string.upper() in output, "We receive the correct upper-cased input string"
  '';
}
